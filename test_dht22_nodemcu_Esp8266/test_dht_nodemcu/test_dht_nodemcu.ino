#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* device_name = "Office";   // Change this value for each WiFi devices

int upload_interval = 1000;       // Uploading interval of DHT22 data in miliseconds.

/*    --------  Constant values -----------        */

const char* ssid = "WiFi560-AP";              // RPi3's AP name
const char* password = "Shane_wifi560";       // RPi3's AP password
const char* mqtt_server = "172.24.1.1";   // Server has been built on the router(RPi 3) itself

// Include and Configure DHT11 SENSOR

#include "DHT.h"
#define DHTPIN 13     // D7, GPIO13
#define DHTTYPE DHT11   // DHT 22         

// Global variables
DHT dht(DHTPIN, DHTTYPE);

float humidity, temp_c, temp_f, heatindex;

WiFiClient espClient;              //creat client that can connect to specific IP address and port.
PubSubClient client(espClient);    //Creates a partially initialised client instance. Before it can be used, the server details must be configured.

long lastMsg = 0;

void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);     //the server details must be configured: the address of server is 172.24.1.1, port is 1883.
  client.setCallback(callback);           //
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {   //Return the connection status.
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());   // Gets the WiFi shield's IP address
  Serial.println("");
}

void callback(char* topic, byte* payload, unsigned int length) {     //If the client is used to subscribe to topics, a callback function must be provided in the constructor.
  Serial.print("Message arrived [");                                 // This function is called when new messages arrive at the client.
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println("");

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {                            //Checks whether the client is connected to the server.
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {             //Connects the ESP8266Client.    ////////////////////////
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("overall_topic", "hello world");   ////////////////////////////////////
      // ... and resubscribe
      client.subscribe("common");         ////////////////////////////
    }
    else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}

void loop() {
  // turn relays off after 90 sec
  //  if ((millis() - t_start_open > 90 * 1000) && !strncmp(w_open_state, "ON", 2)){
  //    w_open_state = "OFF";
  //    digitalWrite(PIN_W_OPEN, LOW);
  //  }
  //  if ((millis() - t_start_close > 90 * 1000) && !strncmp(w_close_state, "ON", 2)){
  //    w_close_state = "OFF";
  //    digitalWrite(PIN_W_CLOSE, LOW);
  //  }

  if (!client.connected()) {
    reconnect();
  }
  client.loop();   //This should be called regularly to allow the client to process incoming messages and maintain its connection to the server.

  long now = millis();    //Returns the number of milliseconds since the Arduino board began running the current program. This number will overflow (go back to zero), after approximately 50 days.

  if (now - lastMsg > upload_interval) {
    lastMsg = now;
    ReadDHT();
    char* buf_temp = new char[10];    ////////////////////////
    char* buf_hum = new char[10];     ///////////////////////

    dtostrf(temp_c, 5, 2, buf_temp);     //convert a float to a char array
    dtostrf(humidity, 5, 2, buf_hum);

    // publish temperature value
    client.publish("temperature", buf_temp);

    // publish humidity value
    client.publish("humidity", buf_hum);
  }
  else {
    delay(400);   // Loop function takes about 300ms, so 400 ms is enough.
  }
}

void ReadDHT() {
  // Read humidity (percent)
  humidity = dht.readHumidity();
  // Read temperature as Celsius
  temp_c = dht.readTemperature();
  // Read temperature as Fahrenheit
  //  temp_f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(humidity) || isnan(temp_c)) {
    Serial.println("Failed to read from DHT sensor :-(");
    return;                /////////////////////////////////////////////////
  }

  // Compute heat index
  // Must send in temp in Fahrenheit!
  //  heatindex = dht.computeHeatIndex(temp_f, humidity);

  Serial.print("Temp:  ");
  Serial.print(temp_c);
  Serial.print(",  Humidity:");
  Serial.println(humidity);
}
